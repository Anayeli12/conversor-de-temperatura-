package com.domain.anayeli.miapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText cent, faren;
    Button convcent, Convfaren;
    TextView Dtcent, Dtfaren;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("cuartoA", "Cedeño Giler Anayeli mireya");
        cent = (EditText)findViewById(R.id.txcent);
        faren = (EditText)findViewById(R.id.txfaren);
        convcent = (Button) findViewById(R.id.botonfaren);
        Convfaren = (Button)findViewById(R.id.botoncent);
        Dtcent = (TextView)findViewById(R.id.LBLCenti);
        Dtfaren =(TextView)findViewById(R.id.LBLFaren);

        Convfaren.setOnClickListener((View.OnClickListener) this);
        convcent.setOnClickListener((View.OnClickListener) this);
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.botonfaren:
                if (faren.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.Validacion), Toast.LENGTH_LONG).show();
                }else {
                    Double faenh = Double.valueOf(faren.getText().toString());
                    Double Centih = (faenh - 32) / 1.8;
                    Toast.makeText(MainActivity.this, String.valueOf(Centih)+ " ºC", Toast.LENGTH_LONG).show();
                    Dtcent.setText(String.valueOf(Centih)+ " ºC");
                    Dtfaren.setText("");
                }
                break;

            case R.id.botoncent:
                if (cent.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.Validacion), Toast.LENGTH_LONG).show();
                }else{
                    Double Centi = Double.valueOf(cent.getText().toString());
                    Double Farenh = (Centi *1.8 ) + 32;
                    Toast.makeText(MainActivity.this, String.valueOf(Farenh)+ " ºF", Toast.LENGTH_LONG).show();
                    Dtfaren.setText(String.valueOf(Farenh)+ " ºF");
                    cent.setText("");
                }
                break;

        }
    }
}

